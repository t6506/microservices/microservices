package org.example.stores.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.stores.dto.TerminalsDto;
import org.example.stores.entity.Store;
import org.example.stores.entity.Terminal;
import org.example.stores.repository.StoreRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class StoreService {
    private final StoreRepository repository;

    public TerminalsDto getTerminalsByStoreId(final String storeId) {
        Store store = repository.getReferenceById(storeId);
        List<String> terminals = store.getTerminals().stream()
            .map(Terminal::getId)
            .collect(Collectors.toList());
        return new TerminalsDto(terminals);
    }
}
