package org.example.stores.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.stores.dto.TerminalsDto;
import org.example.stores.service.StoreService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class StoreController {
    private final StoreService service;

    @GetMapping("{storeId}/terminals")
    public TerminalsDto getTerminalsByStoreId(@PathVariable String storeId) {
        return service.getTerminalsByStoreId(storeId);
    }
}
