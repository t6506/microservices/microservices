INSERT INTO stores (id, name, longitude_int, longitude_frac, latitude_int, latitude_frac)
VALUES ('e8e6f770-6053-4fa7-8d66-c3219f49ab48', 'METRO', 47, 245616, 39, 845035),
       ('84809940-77fb-4a3e-9631-176ca663c65b', 'MAGNIT', 47, 236825, 39, 799149),
       ('b3a319b2-c192-42fb-aaf0-61fc535a1c42', 'IKEA', 57, 175543, 65, 656481);


INSERT INTO terminals (id, store_id)
VALUES ('3c772966-52aa-4177-952d-0f0f55b3dcd9', '84809940-77fb-4a3e-9631-176ca663c65b'),
       ('a874ceef-9338-4535-b211-a7a23b50a340', '84809940-77fb-4a3e-9631-176ca663c65b'),
       ('b541abd9-1d1f-4d32-842b-8a44d5f071e3', '84809940-77fb-4a3e-9631-176ca663c65b'),
       ('955f0099-297e-4ab3-9114-190cf9c7ddca', '84809940-77fb-4a3e-9631-176ca663c65b');


