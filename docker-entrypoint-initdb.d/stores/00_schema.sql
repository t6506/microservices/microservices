CREATE TABLE stores
(
    id             TEXT PRIMARY KEY,
    name           TEXT    NOT NULL,
    longitude_int  INTEGER NOT NULL CHECK ( longitude_int > -180 AND longitude_int < 180),
    longitude_frac INTEGER NOT NULL CHECK ( longitude_frac > 0),
    latitude_int   INTEGER NOT NULL CHECK ( latitude_int > -180 AND latitude_int < 180),
    latitude_frac  INTEGER NOT NULL CHECK ( latitude_frac > 0)
);

CREATE TABLE terminals
(
    id TEXT PRIMARY KEY,
    store_id TEXT REFERENCES stores(id)
);