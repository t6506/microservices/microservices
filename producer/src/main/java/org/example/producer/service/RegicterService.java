package org.example.producer.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.producer.dto.PaymentDto;
import org.example.producer.model.PaymentMessage;
import org.example.producer.model.PaymentStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class RegicterService {
    private final KafkaOperations<String, PaymentMessage> template;
    @Value("${topic.for-processing}")
    private String topicForSend;

    public PaymentDto register(final PaymentDto payment) {
        payment.setRegistered(Instant.now().toEpochMilli());
        payment.setStatus(PaymentStatus.INPROGRESS);
        log.info("received payment: {}", payment);
        final PaymentMessage message = PaymentMessage.builder()
            .id(UUID.randomUUID().toString())
            .payment(payment)
            .created(Instant.now().toEpochMilli())
            .build();
        template.send(topicForSend, message).addCallback(
            result -> {
                log.info("sent to kafka \"{}\": {}", topicForSend, message);
                            },
            ex -> {
                log.error("can't send to kafka \"{}\": {}", topicForSend, message, ex);
                            }
        );
        return payment;
    }
}
