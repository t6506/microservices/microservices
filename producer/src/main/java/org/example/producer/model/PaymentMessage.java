package org.example.producer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.producer.dto.PaymentDto;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PaymentMessage {
    private String id;
    private PaymentDto payment;
    private long created;


}
