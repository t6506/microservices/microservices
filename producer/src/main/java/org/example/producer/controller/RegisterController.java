package org.example.producer.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.producer.dto.PaymentDto;
import org.example.producer.service.RegicterService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
@Validated
public class RegisterController {
    private final RegicterService service;

    @PostMapping
    public PaymentDto register(@Valid @RequestBody final PaymentDto payment) {
        return service.register(payment);
    }
}
