package org.example.aggregator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class TerminalsIdsDto {
    private List<String> terminalsIds;
}
