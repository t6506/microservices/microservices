package org.example.aggregator.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.aggregator.client.HistoryServiceClient;
import org.example.aggregator.client.StoresServiceClient;
import org.example.aggregator.dto.PaymentDto;
import org.example.aggregator.dto.TerminalsIdsDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Slf4j
@Service
@RequiredArgsConstructor
public class AggregatorService {
    private final HistoryServiceClient historyClient;
    private final StoresServiceClient storesClient;

    public List<PaymentDto> getPaymentStatisticsByStoreId(final String storeId) {
        log.info("request to stores by store_id: {}", storeId);
        final List<String> terminalIds = storesClient.getTerminalsByStoreId(storeId).getTerminals();
        log.info("response from stores: {}", terminalIds);
        final TerminalsIdsDto terminalsIdsDto = new TerminalsIdsDto(terminalIds);
        log.info("request to history by terminal_ids: {}", terminalsIdsDto);
        final List<PaymentDto> payments = historyClient.getPaymentStatisticsByTerminalIds(terminalsIdsDto);
        log.info("response from history: {}", payments);
        return payments;
    }
}
