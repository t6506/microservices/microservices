package org.example.aggregator.client;

import org.example.aggregator.dto.PaymentDto;
import org.example.aggregator.dto.TerminalsIdsDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.List;

@FeignClient("history")
public interface HistoryServiceClient {

    @GetMapping("{terminalId}/payments")
    List<PaymentDto> getPaymentStatisticsByTerminalId(@PathVariable String terminalId);

    @PostMapping("payments")
    List<PaymentDto> getPaymentStatisticsByTerminalIds(@RequestBody TerminalsIdsDto terminalsIdsDto);
}
