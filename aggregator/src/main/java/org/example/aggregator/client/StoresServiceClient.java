package org.example.aggregator.client;

import org.example.aggregator.dto.TerminalsDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("stores")
public interface StoresServiceClient {

    @GetMapping("/{storeId}/terminals")
    TerminalsDto getTerminalsByStoreId(@PathVariable final String storeId);
}
