package org.example.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class ZookeeperConfigWriter {
    public static final String CONNECT_STRING = Optional
        .ofNullable(System.getenv("CFG_ZOOKEEPER_CONNECT"))
        .orElse("localhost:2181");
    public static final String PATH_PROPERTIES = Optional
        .ofNullable(System.getenv("CFG_PATH_PROPERTIES"))
        .orElse("config/current.properties");
    public static final String ROOT_PATH = "/config";

    public static void main(String[] args) throws Exception {
        try (
            final CuratorFramework curator = CuratorFrameworkFactory
                .newClient(CONNECT_STRING, new ExponentialBackoffRetry(50, 10));
        ) {
            curator.start();
            final Map<String, String> properties = getProperties(PATH_PROPERTIES);
            writeProperties(curator, ROOT_PATH, properties);
        }
    }

    private static Map<String, String> getProperties(final String pathToFile) throws IOException {
        log.info("read property from file \"{}\"", pathToFile);
        final Map<String, String> properties = Files.readAllLines(Paths.get(pathToFile))
            .stream()
            .map(o -> o.split("="))
            .filter(o -> o.length == 2)
            .peek(o -> log.info("read: {}={}", o[0], o[1]))
            .collect(Collectors
                .toMap(
                    o -> o[0].replace('.', '/'),
                    o -> o[1]));
        log.info("from file \"{}\" read {} properties", pathToFile, properties.size());
        return properties;
    }

    private static void writeProperties(final CuratorFramework curator, final String rootPath,
                                        final Map<String, String> properties) throws Exception {
        log.info("write property to \"{}\"", rootPath);
        int count = 0;
        String pathToProperty;
        for (Map.Entry<String, String> property : properties.entrySet()) {
            pathToProperty = rootPath + "/" + property.getKey();
            curator.createContainers(pathToProperty);
            final byte[] currentValue = curator.getData().forPath(pathToProperty);
            final byte[] newValue = property.getValue().getBytes(StandardCharsets.UTF_8);
            if (!Arrays.equals(currentValue, newValue)) {
                curator.setData().forPath(pathToProperty, newValue);
                log.info("write: {}" + "=" + "{}", pathToProperty, property.getValue());
                count++;
            }
        }
        log.info("to \"{}\" written {} properties", rootPath, count);
    }
}
