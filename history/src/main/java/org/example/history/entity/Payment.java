package org.example.history.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.example.history.model.PaymentStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.time.Instant;

@Entity(name = "payments")
@Getter
@Setter
@ToString
public class Payment {
    @Id
    private String id;
    @Column(name = "terminal_id")
    private String terminalId;
    @Column(name = "card_number")
    private String cardNumber;
    private long amount;
    private Instant registered;
    @Enumerated(EnumType.STRING)
    private PaymentStatus status;
}
