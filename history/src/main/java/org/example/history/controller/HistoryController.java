package org.example.history.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.history.dto.PaymentDto;
import org.example.history.dto.TerminalsIdsDto;
import org.example.history.service.HistoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class HistoryController {
    private final HistoryService service;

    @GetMapping("{terminalId}/payments")
    public List<PaymentDto> getPaymentsByTerminalId(@PathVariable String terminalId) {
        return service.getPaymentsByTerminalId(terminalId);
    }

    @PostMapping("payments")
    public List<PaymentDto> getPaymentsByTerminalIds(@RequestBody TerminalsIdsDto terminalsIdsDto) {
        return service.getPaymentsByTerminalIds(terminalsIdsDto);
    }
}
