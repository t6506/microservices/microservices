package org.example.consumer.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.example.consumer.dto.PaymentDto;
import org.example.consumer.model.PaymentMessage;
import org.example.consumer.model.PaymentStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProcessingService {
    private final KafkaOperations<String, PaymentMessage> template;
    @Value("${topic.after-processing}")
    private String topicForSend;


    @KafkaListener(topics = "${topic.for-processing}")
    public void listen(final ConsumerRecord<String, PaymentMessage> message, final Acknowledgment acknowledgment) {
        log.info("received: {}", message);
        final PaymentDto received = message.value().getPayment();
        final PaymentDto processed = process(received);
        acknowledgment.acknowledge();
        send(processed);
    }

    private PaymentDto process(final PaymentDto paymentDto) {
        paymentDto.setStatus(PaymentStatus.SUCCESS);
        log.info("payment processed: {}", paymentDto);
        return paymentDto;
    }

    private void send(final PaymentDto payment) {
        final PaymentMessage message = PaymentMessage.builder()
            .id(UUID.randomUUID().toString())
            .payment(payment)
            .created(Instant.now().toEpochMilli())
            .build();
        template.send(topicForSend, message).addCallback(
            result -> {
                log.info("sent to kafka \"{}\": {}", topicForSend, message);
            },
            ex -> {
                log.error("can't send to kafka \"{}\": {}", topicForSend, message, ex);
            }
        );
    }
}
