package org.example.consumer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.consumer.model.PaymentStatus;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaymentDto {
    private String id = UUID.randomUUID().toString();
    private String terminalId;
    private String cardNumber;
    private long amount;
    private long registered;
    private PaymentStatus status;
}
